module MarsRover
  RSpec.describe Rover do
    let(:north_orientation) { Orientation.north }
    let(:south_orientation) { Orientation.south }
    let(:east_orientation) { Orientation.east }
    let(:west_orientation) { Orientation.west }

    describe 'movement' do
      it 'with position x 1, y 1 and orientation north has position x 1, y 2 and orientation north when it moves' do
        position = Position.new(1, 1, north_orientation)
        rover = Rover.new(position)
        next_position = Position.new(1, 2, north_orientation)

        rover.move!

        expect(rover.position).to eq(next_position)
      end

      it 'with position x 1, y 1 and orientation east has position x 2, y 1 and orientation east when it moves' do
        position = Position.new(1, 1, east_orientation)
        rover = Rover.new(position)
        next_position = Position.new(2, 1, east_orientation)

        rover.move!

        expect(rover.position).to eq(next_position)
      end

      it 'with position x 1, y 1 and orientation south has position x 1, y 0 and orientation south when it moves' do
        position = Position.new(1, 1, south_orientation)
        rover = Rover.new(position)
        next_position = Position.new(1, 0, south_orientation)

        rover.move!

        expect(rover.position).to eq(next_position)
      end

      it 'with position x 1, y 1 and orientation west has position x 0, y 1 and orientation west when it moves' do
        position = Position.new(1, 1, west_orientation)
        rover = Rover.new(position)
        next_position = Position.new(0, 1, west_orientation)

        rover.move!

        expect(rover.position).to eq(next_position)
      end
    end

    context "with position's orientation towards north" do
      let(:position) { Position.new(1, 1, north_orientation) }
      let(:rover) { Rover.new(position) }

      it "has position's orientation towards west when spun left" do
        rover.spin_left!
        expected_position = Position.new(1, 1, west_orientation)

        expect(rover.position).to eq(expected_position)
      end

      it "has position's orientation towards east when spun right" do
        rover.spin_right!
        expected_position = Position.new(1, 1, east_orientation)

        expect(rover.position).to eq(expected_position)
      end
    end

    context "with position's orientation towards east" do
      let(:position) { Position.new(1, 1, east_orientation) }
      let(:rover) { Rover.new(position) }

      it "has position's orientation towards west when spun left" do
        rover.spin_left!
        expected_position = Position.new(1, 1, north_orientation)

        expect(rover.position).to eq(expected_position)
      end

      it "has position's orientation towards south when spun right" do
        rover.spin_right!
        expected_position = Position.new(1, 1, south_orientation)

        expect(rover.position).to eq(expected_position)
      end
    end

    context "with position's orientation towards south" do
      let(:position) { Position.new(1, 1, south_orientation) }
      let(:rover) { Rover.new(position) }

      it "has position's orientation towards east when spun left" do
        rover.spin_left!
        expected_position = Position.new(1, 1, east_orientation)

        expect(rover.position).to eq(expected_position)
      end

      it "has position's orientation towards west when spun right" do
        rover.spin_right!
        expected_position = Position.new(1, 1, west_orientation)

        expect(rover.position).to eq(expected_position)
      end
    end

    context "with position's orientation towards west" do
      let(:position) { Position.new(1, 1, west_orientation) }
      let(:rover) { Rover.new(position) }

      it "has position's orientation towards south when spun left" do
        rover.spin_left!
        expected_position = Position.new(1, 1, south_orientation)

        expect(rover.position).to eq(expected_position)
      end

      it "has position's orientation towards west when spun right" do
        rover.spin_right!
        expected_position = Position.new(1, 1, north_orientation)

        expect(rover.position).to eq(expected_position)
      end
    end

  end
end