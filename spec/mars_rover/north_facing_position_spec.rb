module MarsRover
  RSpec.describe NorthFacingPosition do

    context 'with x 1, y 1 when compared for equality with' do
      let(:north_facing_position) { NorthFacingPosition.new(1, 1) }

      it 'nil is not equal' do
        expect(north_facing_position == nil).to eq(false)
      end

      it 'itself is equal' do
        expect(north_facing_position == north_facing_position).to eq(true)
      end

      it 'other north facing position with same coordinates is equal' do
        other_north_facing_position = NorthFacingPosition.new(1, 1)
        expect(north_facing_position == other_north_facing_position).to eq(true)
      end

      it 'other north facing position with different coordinates is not equal' do
        other_north_facing_position = NorthFacingPosition.new(2, 2)
        expect(north_facing_position == other_north_facing_position).to eq(false)
      end

      it 'anything other than north facing position is not equal' do
        expect(north_facing_position == Object.new).to eq(false)
      end

      it 'other north facing position of same coordinates is symmetrically equal' do
        other_north_facing_position = NorthFacingPosition.new(1, 1)
        expect(other_north_facing_position == north_facing_position).to eq(true)
      end
    end

    it 'with same coordinates have the same hash' do
      north_facing_position = NorthFacingPosition.new(1, 1)
      other_north_facing_position = NorthFacingPosition.new(1, 1)
      expect(north_facing_position.hash).to eq(other_north_facing_position.hash)
    end

    describe 'has next position' do
      it 'as x 1, y 2 when initially at x 1, y 1' do
        north_facing_position = NorthFacingPosition.new(1, 1)
        expect(north_facing_position.next).to eq(NorthFacingPosition.new(1, 2))
      end

      it 'as x 2, y 3 when initially at x 2, y 2' do
        north_facing_position = NorthFacingPosition.new(2, 2)
        expect(north_facing_position.next).to eq(NorthFacingPosition.new(2, 3))
      end
    end

    describe 'has left position as' do
      it 'as West with x 1, y 1 when it is initially at x 1, y 1' do
        north_facing_position = NorthFacingPosition.new(1, 1)
        expect(north_facing_position.left).to eq(WestFacingPosition.new(1, 1))
      end

      it 'as West with x 2, y 2 when it is initially at x 2, y 2' do
        north_facing_position = NorthFacingPosition.new(2, 2)
        expect(north_facing_position.left).to eq(WestFacingPosition.new(2, 2))
      end
    end

    describe 'has right position as' do
      it 'as East with x 1, y 1 when it is initially at x 1, y 1' do
        north_facing_position = NorthFacingPosition.new(1, 1)
        expect(north_facing_position.right).to eq(EastFacingPosition.new(1, 1))
      end

      it 'as East with x 2, y 2 when it is initially at x 2, y 2' do
        north_facing_position = NorthFacingPosition.new(2, 2)
        expect(north_facing_position.right).to eq(EastFacingPosition.new(2, 2))
      end
    end

  end
end