module MarsRover
  RSpec.describe EastFacingPosition do

    context 'with x 1, y 1 when compared for equality with' do
      let(:east_facing_position) { EastFacingPosition.new(1, 1) }

      it 'nil is not equal' do
        expect(east_facing_position == nil).to eq(false)
      end

      it 'itself is equal' do
        expect(east_facing_position == east_facing_position).to eq(true)
      end

      it 'other east facing position with same coordinates is equal' do
        other_east_facing_position = EastFacingPosition.new(1, 1)
        expect(east_facing_position == other_east_facing_position).to eq(true)
      end

      it 'other east facing position with different coordinates is not equal' do
        other_east_facing_position = EastFacingPosition.new(2, 2)
        expect(east_facing_position == other_east_facing_position).to eq(false)
      end

      it 'anything other than east facing position is not equal' do
        expect(east_facing_position == Object.new).to eq(false)
      end

      it 'other east facing position of same coordinates is symmetrically equal' do
        other_east_facing_position = EastFacingPosition.new(1, 1)
        expect(other_east_facing_position == east_facing_position).to eq(true)
      end
    end

    it 'with same coordinates have the same hash' do
      east_facing_position = EastFacingPosition.new(1, 1)
      other_east_facing_position = EastFacingPosition.new(1, 1)
      expect(east_facing_position.hash).to eq(other_east_facing_position.hash)
    end

    describe 'has next position' do
      it 'as x 2, y 1 when initially at x 1, y 1' do
        east_facing_position = EastFacingPosition.new(1, 1)
        expect(east_facing_position.next).to eq(EastFacingPosition.new(2, 1))
      end

      it 'as x 3, y 2 when initially at x 2, y 2' do
        east_facing_position = EastFacingPosition.new(2, 2)
        expect(east_facing_position.next).to eq(EastFacingPosition.new(3, 2))
      end
    end

    describe 'has left position as' do
      it 'North with x 1, y 1 when it is initially at x 1, y 1' do
        east_facing_position = EastFacingPosition.new(1, 1)
        expect(east_facing_position.left).to eq(NorthFacingPosition.new(1, 1))
      end

      it 'North with x 2, y 2 when it is initially at x 2, y 2' do
        east_facing_position = EastFacingPosition.new(2, 2)
        expect(east_facing_position.left).to eq(NorthFacingPosition.new(2, 2))
      end
    end

    describe 'has right position as' do
      it 'South with x 1, y 1 when it is initially at x 1, y 1' do
        east_facing_position = EastFacingPosition.new(1, 1)
        expect(east_facing_position.right).to eq(SouthFacingPosition.new(1, 1))
      end

      it 'South with x 2, y 2 when it is initially at x 2, y 2' do
        east_facing_position = EastFacingPosition.new(2, 2)
        expect(east_facing_position.right).to eq(SouthFacingPosition.new(2, 2))
      end
    end

  end
end