module MarsRover
  RSpec.describe SouthFacingPosition do

    context 'with x 1, y 1 when compared for equality with' do
      let(:south_facing_position) { SouthFacingPosition.new(1, 1) }

      it 'nil is not equal' do
        expect(south_facing_position == nil).to eq(false)
      end

      it 'itself is equal' do
        expect(south_facing_position == south_facing_position).to eq(true)
      end

      it 'other south facing position with same coordinates is equal' do
        other_south_facing_position = SouthFacingPosition.new(1, 1)
        expect(south_facing_position == other_south_facing_position).to eq(true)
      end

      it 'other south facing position with different coordinates is not equal' do
        other_south_facing_position = SouthFacingPosition.new(2, 2)
        expect(south_facing_position == other_south_facing_position).to eq(false)
      end

      it 'anything other than south facing position is not equal' do
        expect(south_facing_position == Object.new).to eq(false)
      end

      it 'other south facing position of same coordinates is symmetrically equal' do
        other_south_facing_position = SouthFacingPosition.new(1, 1)
        expect(other_south_facing_position == south_facing_position).to eq(true)
      end
    end

    it 'with same coordinates have the same hash' do
      south_facing_position = SouthFacingPosition.new(1, 1)
      other_south_facing_position = SouthFacingPosition.new(1, 1)
      expect(south_facing_position.hash).to eq(other_south_facing_position.hash)
    end

    describe 'has next position' do
      it 'as x 1, y 0 when initially at x 1, y 1' do
        south_facing_position = SouthFacingPosition.new(1, 1)
        expect(south_facing_position.next).to eq(SouthFacingPosition.new(1, 0))
      end

      it 'as x 2, y 1 when initially at x 2, y 2' do
        south_facing_position = SouthFacingPosition.new(2, 2)
        expect(south_facing_position.next).to eq(SouthFacingPosition.new(2, 1))
      end
    end

    describe 'has left position as' do
      it 'East with x 1, y 1 when it is initially at x 1, y 1' do
        south_facing_position = SouthFacingPosition.new(1, 1)
        expect(south_facing_position.left).to eq(EastFacingPosition.new(1, 1))
      end

      it 'East with x 2, y 2 when it is initially at x 2, y 2' do
        south_facing_position = SouthFacingPosition.new(2, 2)
        expect(south_facing_position.left).to eq(EastFacingPosition.new(2, 2))
      end
    end

    describe 'has right position as' do
      it 'West with x 1, y 1 when it is initially at x 1, y 1' do
        south_facing_position = SouthFacingPosition.new(1, 1)
        expect(south_facing_position.right).to eq(WestFacingPosition.new(1, 1))
      end

      it 'West with x 2, y 2 when it is initially at x 2, y 2' do
        south_facing_position = SouthFacingPosition.new(2, 2)
        expect(south_facing_position.right).to eq(WestFacingPosition.new(2, 2))
      end
    end

  end
end