module MarsRover
  RSpec.describe Position do

    context 'of x coordinate 0, y coordinate 0 and north orientation when compared for equality with' do
      let(:orientation) { Orientation.north }
      let(:position) { Position.new(0, 0, orientation) }

      it 'nil is not equal' do
        expect(position == nil).to eq(false)
      end

      it 'itself is equal' do
        expect(position == position).to eq(true)
      end

      it 'other Position of same states is equal' do
        other_position = Position.new(0, 0, orientation)
        expect(position == other_position).to eq(true)
      end

      it 'other Position of different states is not equal' do
        other_position = Position.new(0, 2, orientation)
        expect(position == other_position).to eq(false)
      end

      it 'anything other than position is not equal' do
        expect(position == Object.new).to eq(false)
      end

      it 'other Position of same states is symmetrically equal' do
        other_position = Position.new(0, 0, orientation)
        expect(other_position == position).to eq(true)
      end
    end

    it 'with same states have same hash' do
      orientation = Orientation.north
      position = Position.new(1, 1, orientation)
      other_position = Position.new(1, 1, orientation)

      expect(position.hash).to eq(other_position.hash)
    end

    describe 'has next position as' do
      it 'x 1, y 2 and orientation north when it is originally x 1, y 1 and orientation north' do
        orientation = Orientation.north
        position = Position.new(1, 1, orientation)

        expect(position.next).to eq(Position.new(1, 2, orientation))
      end

      it 'x 2, y 1 and orientation east when it is originally x 1, y 1 and orientation east' do
        orientation = Orientation.east
        position = Position.new(1, 1, orientation)

        expect(position.next).to eq(Position.new(2, 1, orientation))
      end

      it 'x 1, y 0 and orientation south when it is originally x 1, y 1 and orientation south' do
        orientation = Orientation.south
        position = Position.new(1, 1, orientation)

        expect(position.next).to eq(Position.new(1, 0, orientation))
      end

      it 'x 0, y 1 and orientation west when it is originally x 1, y 1 and orientation west' do
        orientation = Orientation.west
        position = Position.new(1, 1, orientation)

        expect(position.next).to eq(Position.new(0, 1, orientation))
      end
    end

    describe 'with orientation towards north' do
      let(:orientation) { Orientation.north }
      let(:position) { Position.new(1, 1, orientation) }

      it 'has orientation towards west when oriented left' do
        new_orientation = Orientation.west
        expect(position.with_left_orientation).to eq(Position.new(1, 1, new_orientation))
      end

      it 'has orientation towards east when oriented right' do
        new_orientation = Orientation.east
        expect(position.with_right_orientation).to eq(Position.new(1, 1, new_orientation))
      end
    end

    describe 'with orientation towards east' do
      let(:orientation) { Orientation.east }
      let(:position) { Position.new(1, 1, orientation) }

      it 'has orientation towards north when oriented left' do
        new_orientation = Orientation.north
        expect(position.with_left_orientation).to eq(Position.new(1, 1, new_orientation))
      end

      it 'has orientation towards south when oriented right' do
        new_orientation = Orientation.south
        expect(position.with_right_orientation).to eq(Position.new(1, 1, new_orientation))
      end
    end

    describe 'with orientation towards south' do
      let(:orientation) { Orientation.south }
      let(:position) { Position.new(1, 1, orientation) }

      it 'has orientation towards east when oriented left' do
        new_orientation = Orientation.east
        expect(position.with_left_orientation).to eq(Position.new(1, 1, new_orientation))
      end

      it 'has orientation towards west when oriented right' do
        new_orientation = Orientation.west
        expect(position.with_right_orientation).to eq(Position.new(1, 1, new_orientation))
      end
    end

    describe 'with orientation towards west' do
      let(:orientation) { Orientation.west }
      let(:position) { Position.new(1, 1, orientation) }

      it 'has orientation towards south when oriented left' do
        new_orientation = Orientation.south
        expect(position.with_left_orientation).to eq(Position.new(1, 1, new_orientation))
      end

      it 'has orientation towards north when oriented right' do
        new_orientation = Orientation.north
        expect(position.with_right_orientation).to eq(Position.new(1, 1, new_orientation))
      end
    end

  end
end