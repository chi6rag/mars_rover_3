module MarsRover
  RSpec.describe Orientation do

    it 'towards north sets North as its cardinal direction' do
      north_orientation = Orientation.north
      expect(north_orientation.cardinal_direction).to eq(NORTH)
    end

    it 'towards east sets East as its cardinal direction' do
      east_orientation = Orientation.east
      expect(east_orientation.cardinal_direction).to eq(EAST)
    end

    it 'towards south sets South as its cardinal direction' do
      south_orientation = Orientation.south
      expect(south_orientation.cardinal_direction).to eq(SOUTH)
    end

    it 'towards west sets West as its cardinal direction' do
      west_orientation = Orientation.west
      expect(west_orientation.cardinal_direction).to eq(WEST)
    end

    describe 'towards North when compared for equality with' do
      let(:north_orientation) { Orientation.north }
      let(:other_north_orientation) { Orientation.north }
      let(:south_orientation) { Orientation.south }

      it 'nil is not equal' do
        expect(north_orientation == nil).to eq(false)
      end

      it 'itself is equal' do
        expect(north_orientation == north_orientation).to eq(true)
      end

      it 'other North orientation is equal' do
        expect(north_orientation == other_north_orientation).to eq(true)
      end

      it 'South orientation is equal' do
        expect(north_orientation == south_orientation).to eq(false)
      end

      it 'anything other than orientation is not equal' do
        expect(north_orientation == Object.new).to eq(false)
      end

      it 'other North orientation is symmetrically equal' do
        expect(other_north_orientation == north_orientation).to eq(true)
      end
    end

    it 'towards North has same hash as other orientation towards North' do
      north_orientation = Orientation.north
      other_north_orientation = Orientation.north
      expect(north_orientation.hash).to eq(other_north_orientation.hash)
    end

    describe 'towards East when compared for equality with' do
      let(:east_orientation) { Orientation.east }
      let(:other_east_orientation) { Orientation.east }
      let(:west_orientation) { Orientation.west }

      it 'nil is not equal' do
        expect(east_orientation == nil).to eq(false)
      end

      it 'itself is equal' do
        expect(east_orientation == east_orientation).to eq(true)
      end

      it 'other East orientation is equal' do
        expect(east_orientation == other_east_orientation).to eq(true)
      end

      it 'West orientation is equal' do
        expect(east_orientation == west_orientation).to eq(false)
      end

      it 'anything other than orientation is not equal' do
        expect(east_orientation == Object.new).to eq(false)
      end

      it 'other East orientation is symmetrically equal' do
        expect(other_east_orientation == east_orientation).to eq(true)
      end
    end

    it 'towards East has same hash as other orientation towards East' do
      east_orientation = Orientation.east
      other_east_orientation = Orientation.east
      expect(east_orientation.hash).to eq(other_east_orientation.hash)
    end

    describe 'towards South when compared for equality with' do
      let(:south_orientation) { Orientation.south }
      let(:other_south_orientation) { Orientation.south }
      let(:north_orientation) { Orientation.north }

      it 'nil is not equal' do
        expect(south_orientation == nil).to eq(false)
      end

      it 'itself is equal' do
        expect(south_orientation == south_orientation).to eq(true)
      end

      it 'other South orientation is equal' do
        expect(south_orientation == other_south_orientation).to eq(true)
      end

      it 'North orientation is equal' do
        expect(south_orientation == north_orientation).to eq(false)
      end

      it 'anything other than orientation is not equal' do
        expect(south_orientation == Object.new).to eq(false)
      end

      it 'other North orientation is symmetrically equal' do
        expect(other_south_orientation == south_orientation).to eq(true)
      end
    end

    it 'towards South has same hash as other orientation towards South' do
      south_orientation = Orientation.south
      other_south_orientation = Orientation.south
      expect(south_orientation.hash).to eq(other_south_orientation.hash)
    end

    describe 'towards West when compared for equality with' do
      let(:west_orientation) { Orientation.west }
      let(:other_west_orientation) { Orientation.west }
      let(:east_orientation) { Orientation.east }

      it 'nil is not equal' do
        expect(west_orientation == nil).to eq(false)
      end

      it 'itself is equal' do
        expect(west_orientation == west_orientation).to eq(true)
      end

      it 'other West orientation is equal' do
        expect(west_orientation == other_west_orientation).to eq(true)
      end

      it 'East orientation is equal' do
        expect(west_orientation == east_orientation).to eq(false)
      end

      it 'anything other than orientation is not equal' do
        expect(west_orientation == Object.new).to eq(false)
      end

      it 'other West orientation is symmetrically equal' do
        expect(other_west_orientation == west_orientation).to eq(true)
      end
    end

    it 'towards East has same hash as other orientation towards East' do
      east_orientation = Orientation.east
      other_east_orientation = Orientation.east
      expect(east_orientation.hash).to eq(other_east_orientation.hash)
    end

    context 'left to' do
      let(:north_orientation) { Orientation.north }
      let(:east_orientation) { Orientation.east }
      let(:south_orientation) { Orientation.south }
      let(:west_orientation) { Orientation.west }

      it 'North is Orientation towards West' do
        expect(north_orientation.left).to eq(west_orientation)
      end

      it 'East is Orientation towards North' do
        expect(east_orientation.left).to eq(north_orientation)
      end

      it 'South is Orientation towards East' do
        expect(south_orientation.left).to eq(east_orientation)
      end

      it 'West is Orientation towards South' do
        expect(west_orientation.left).to eq(south_orientation)
      end
    end

    context 'right to' do
      let(:north_orientation) { Orientation.north }
      let(:east_orientation) { Orientation.east }
      let(:south_orientation) { Orientation.south }
      let(:west_orientation) { Orientation.west }

      it 'North is Orientation towards East' do
        expect(north_orientation.right).to eq(east_orientation)
      end
      it 'East is Orientation towards South' do
        expect(east_orientation.right).to eq(south_orientation)
      end
      it 'South is Orientation towards West' do
        expect(south_orientation.right).to eq(west_orientation)
      end
      it 'West is Orientation towards North' do
        expect(west_orientation.right).to eq(north_orientation)
      end
    end

    context 'towards North' do
      let(:north_orientation) { Orientation.north }

      it 'points towards North' do
        expect(north_orientation.points_towards_north?).to eq(true)
      end

      it 'does not point towards East' do
        expect(north_orientation.points_towards_east?).to eq(false)
      end

      it 'does not point towards South' do
        expect(north_orientation.points_towards_south?).to eq(false)
      end

      it 'does not point towards West' do
        expect(north_orientation.points_towards_west?).to eq(false)
      end
    end

    context 'towards East' do
      let(:east_orientation) { Orientation.east }

      it 'does not point towards North' do
        expect(east_orientation.points_towards_north?).to eq(false)
      end

      it 'points towards East' do
        expect(east_orientation.points_towards_east?).to eq(true)
      end

      it 'does not point towards South' do
        expect(east_orientation.points_towards_south?).to eq(false)
      end

      it 'does not point towards West' do
        expect(east_orientation.points_towards_west?).to eq(false)
      end
    end

    context 'towards South' do
      let(:south_orientation) { Orientation.south }

      it 'does not point towards North' do
        expect(south_orientation.points_towards_north?).to eq(false)
      end

      it 'does not point towards East' do
        expect(south_orientation.points_towards_east?).to eq(false)
      end

      it 'points towards South' do
        expect(south_orientation.points_towards_south?).to eq(true)
      end

      it 'does not point towards West' do
        expect(south_orientation.points_towards_west?).to eq(false)
      end
    end

    context 'towards West' do
      let(:west_orientation) { Orientation.west }

      it 'does not point towards North' do
        expect(west_orientation.points_towards_north?).to eq(false)
      end

      it 'does not point towards North' do
        expect(west_orientation.points_towards_east?).to eq(false)
      end

      it 'does not point towards South' do
        expect(west_orientation.points_towards_south?).to eq(false)
      end

      it 'points towards West' do
        expect(west_orientation.points_towards_west?).to eq(true)
      end
    end

  end
end