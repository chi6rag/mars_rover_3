module MarsRover

  # Orientation represents the cardinal directions - North, East, South, West

  class Orientation
    include CardinalDirections

    attr_reader :cardinal_direction

    def initialize(cardinal_direction)
      @cardinal_direction = cardinal_direction
    end

    def self.north
      new(NORTH)
    end

    def self.east
      new(EAST)
    end

    def self.south
      new(SOUTH)
    end

    def self.west
      new(WEST)
    end

    def points_towards_north?
      @cardinal_direction == NORTH
    end

    def points_towards_east?
      @cardinal_direction == EAST
    end

    def points_towards_south?
      @cardinal_direction == SOUTH
    end

    def points_towards_west?
      @cardinal_direction == WEST
    end

    def ==(other)
      return false unless other.is_a?(Orientation)
      return true if self.object_id == other.object_id
      self.cardinal_direction == other.cardinal_direction
    end

    def hash
      [@cardinal_direction].hash
    end

    def right
      current_orientation_index = calculate_current_orientation_index(orientations, @cardinal_direction)
      right_orientation = find_right_orientation(orientations, current_orientation_index)
      Orientation.new(right_orientation)
    end

    def left
      current_orientation_index = calculate_current_orientation_index(orientations, @cardinal_direction)
      left_orientation = find_left_orientation(orientations, current_orientation_index)
      Orientation.new(left_orientation)
    end

    private
    def orientations
      [NORTH, EAST, SOUTH, WEST]
    end

    def calculate_current_orientation_index(orientations, current_cardinal_direction)
      orientations.find_index(current_cardinal_direction)
    end

    def find_right_orientation(orientations, current_orientation_index)
      orientations[current_orientation_index.next.modulo(orientations.length)]
    end

    def find_left_orientation(orientations, current_orientation_index)
      orientations[current_orientation_index.pred]
    end

  end
end