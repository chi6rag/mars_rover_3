module MarsRover

  # x
  # which knows how to find other positions near to itself

  class Position
    attr_reader :x, :y, :orientation

    def initialize(x_coordinate, y_coordinate, orientation)
      @x = x_coordinate
      @y = y_coordinate
      @orientation = orientation
    end

    def ==(other)
      return false unless other.is_a?(Position)
      return true if other.object_id == self.object_id
      self.x == other.x &&
      self.y == other.y &&
      self.orientation == other.orientation
    end

    def hash
      [@x, @y, @orientation].hash
    end

    def next
      if @orientation.points_towards_north?
        next_north_position
      elsif @orientation.points_towards_east?
        next_east_position
      elsif @orientation.points_towards_south?
        next_south_position
      elsif @orientation.points_towards_west?
        next_west_position
      end
    end

    def with_left_orientation
      Position.new(@x, @y, @orientation.left)
    end

    def with_right_orientation
      Position.new(@x, @y, @orientation.right)
    end

    private
    def next_north_position
      Position.new(@x, @y + 1, @orientation)
    end

    def next_east_position
      Position.new(@x + 1, @y, @orientation)
    end

    def next_south_position
      Position.new(@x, @y - 1, @orientation)
    end

    def next_west_position
      Position.new(@x - 1, @y, @orientation)
    end

  end
end