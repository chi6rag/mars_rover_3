module MarsRover
  class SouthFacingPosition
    attr_reader :x, :y

    def initialize(x_coordinates, y_coordinates)
      @x = x_coordinates
      @y = y_coordinates
    end

    def ==(other)
      return false unless other.is_a?(SouthFacingPosition)
      return true if other.object_id == self.object_id
      self.x == other.x && self.y == other.y
    end

    def hash
      [@x, @y].hash
    end

    def next
      SouthFacingPosition.new(@x, @y - 1)
    end

    def left
      EastFacingPosition.new(@x, @y)
    end

    def right
      WestFacingPosition.new(@x, @y)
    end

  end
end