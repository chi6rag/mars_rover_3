module MarsRover
  class EastFacingPosition
    attr_reader :x, :y

    def initialize(x_coordinates, y_coordinates)
      @x = x_coordinates
      @y = y_coordinates
    end

    def ==(other)
      return false unless other.is_a?(EastFacingPosition)
      return true if other.object_id == self.object_id
      self.x == other.x && self.y == other.y
    end

    def hash
      [@x, @y].hash
    end

    def next
      EastFacingPosition.new(@x + 1, @y)
    end

    def left
      NorthFacingPosition.new(@x, @y)
    end

    def right
      SouthFacingPosition.new(@x, @y)
    end

  end
end