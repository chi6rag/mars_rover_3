module MarsRover
  class WestFacingPosition
    attr_reader :x, :y

    def initialize(x_coordinates, y_coordinates)
      @x = x_coordinates
      @y = y_coordinates
    end

    def ==(other)
      return false unless other.is_a?(WestFacingPosition)
      return true if other.object_id == self.object_id
      self.x == other.x && self.y == other.y
    end

    def hash
      [@x, @y].hash
    end

    def next
      WestFacingPosition.new(@x - 1, @y)
    end

    def left
      SouthFacingPosition.new(@x, @y)
    end

    def right
      NorthFacingPosition.new(@x, @y)
    end

  end
end