module MarsRover

  # Rover represents a robot on Mars which knows how to navigate

  class Rover
    attr_reader :position

    def initialize(position)
      @position = position
    end

    def move!
      @position = @position.next
    end

    def spin_left!
      @position = @position.with_left_orientation
    end

    def spin_right!
      @position = @position.with_right_orientation
    end

  end
end