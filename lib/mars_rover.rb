require 'mars_rover/cardinal_directions'
require 'mars_rover/orientation'
require 'mars_rover/west_facing_position'
require 'mars_rover/north_facing_position'
require 'mars_rover/east_facing_position'
require 'mars_rover/south_facing_position'
require 'mars_rover/position'
require 'mars_rover/rover'

module MarsRover
  include CardinalDirections
end